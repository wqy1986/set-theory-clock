package com.paic.arch.interviews;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 王谦益
 *
 */
public class MySetTheoryClock implements TimeConverter {

	/* 
	 * 从测试用例来看，期望的输入数据格式类似00:00:00
	 * 而输出数据的格式类似
	 * Y
	 * OOOO
	 * OOOO
	 * OOOOOOOOOOO
	 * OOOO
	 */
	@Override
	public String convertTime(String aTime) {
		Pattern p = Pattern.compile("(\\d\\d):(\\d\\d):(\\d\\d)"); 
        Matcher m = p.matcher(aTime);  
        if (m.find()) {  
        	int hour = Integer.parseInt(m.group(1));
        	int minute = Integer.parseInt(m.group(2));
        	int second = Integer.parseInt(m.group(3));
        	StringBuffer result = new StringBuffer(50);
        	generateFirstLine(result,second);
        	generateSecondLine(result,hour);
        	generateThirdLine(result,hour);
        	generateFourthLine(result,minute);
        	generateFifthLine(result,minute);
        	
        	return result.toString();
        }  
		return null;
	}
	
	/**
	 * 生成第五行的数据，灯亮是输出Y,灯灭输出O，规则为每1分钟多亮一盏灯
	 * @param result 最终结果
	 * @param minute 分钟
	 * @return
	 */
	private void generateFifthLine(StringBuffer result, int minute) {
		for(int i = 1;i<5;i++){
			result.append(minute%5>=i?"Y":"O");
		}
	}

	/**
	 * 生成第四行的数据，灯亮是输出Y,灯灭输出O,但是编号为3的倍数的灯亮的时候是R，规则为每5分钟多亮一盏灯
	 * @param result 最终结果
	 * @param minute 分钟
	 * @return
	 */
	private void generateFourthLine(StringBuffer result, int minute) {
		for(int i = 1;i<12;i++){
			if(minute/5<i){
				result.append("O");
			}else if(i%3==0){
				result.append("R");
			}else{
				result.append("Y");
			}
		}
		result.append("\r\n");
	}

	/**
	 * 生成第三行的数据，灯亮是输出R,灯灭输出O,规则为每1小时多亮一盏灯
	 * @param result 最终结果
	 * @param second 小时
	 * @return
	 */
	private void generateThirdLine(StringBuffer result, int hour) {
		for(int i = 1;i<5;i++){
			result.append(hour%5>=i?"R":"O");
		}
		result.append("\r\n");
	}

	/**
	 * 生成第二行的数据，灯亮是输出R,灯灭输出O,规则为每5小时多亮一盏灯
	 * @param result 最终结果
	 * @param second 小时
	 * @return
	 */
	private void generateSecondLine(StringBuffer result, int hour) {
		for(int i = 0;i<4;i++){
			result.append(hour>5*i+4?"R":"O");
		}
		result.append("\r\n");
	}

	/**
	 * 生成第一行的数据，偶数秒是输出Y,奇数秒输出O
	 * @param result 最终结果
	 * @param second 秒
	 * @return
	 */
	private void generateFirstLine(StringBuffer result,int second){
		result.append(second%2==1?"O":"Y");
		result.append("\r\n");
	}

}
